package ar.com.rockcodes.rockbookshelf.commands;


import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ar.com.rockcodes.rockbookshelf.RockBookShelf;
import ar.com.rockcodes.rockbookshelf.util.VaultHelper;
import net.md_5.bungee.api.ChatColor;


public class CommandsBookSelf implements CommandExecutor {

	
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		
		if (sender instanceof Player) {
			
			//if(RockTickets.runok ==false) {sender.sendMessage("El plugin quedo automaticamente deshabilitado por falta de conexion");return true;}
			
			if(args.length==0){book_main((Player) sender); return true;}
			else{
				
				if(args[0].equals("reload")){ book_reload((Player)sender); }
				else{
					book_main((Player) sender); return true;
				}
			}
			
        } else {
           sender.sendMessage("You are not player");
           return false;
        }
       
		return false;
	}

	private void book_reload(Player player) {
		
		RockBookShelf.plugin.reload();
		player.sendMessage(ChatColor.GREEN+""+ChatColor.BOLD+"BookShelf reloaded!");
		// TODO Auto-generated method stub
		
	}

	private void book_main(Player player) {
		
		  if(VaultHelper.permission.has(player, "rockbookshelf.command")){
			  RockBookShelf.plugin.menu_book.showMenu(player);  
		  }
		
	}

	
	

}
