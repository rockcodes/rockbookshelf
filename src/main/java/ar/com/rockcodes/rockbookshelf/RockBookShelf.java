package ar.com.rockcodes.rockbookshelf;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.rockbookshelf.commands.CommandsBookSelf;
import ar.com.rockcodes.rockbookshelf.listener.BookShelfListener;
import ar.com.rockcodes.rockbookshelf.util.Menu;
import ar.com.rockcodes.rockbookshelf.util.VaultHelper;


public class RockBookShelf extends JavaPlugin{

	
	public static RockBookShelf plugin ;
	public Menu menu_book; 
	
	
	@Override
	public void onDisable() {

	}

	@Override
	public void onEnable() {
		
		RockBookShelf.plugin = this;
		saveDefaultConfig();
		
		if(!VaultHelper.setupPermissions()){
			getLogger().log(Level.SEVERE, "You need vault to start this plugin");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
			
		}

		plugin.getServer().getPluginManager().registerEvents(new BookShelfListener(), this);
		getCommand("bookshelf").setExecutor(new CommandsBookSelf());
		
		this.menu_book = this.get_menu();
		

	}

	
	
	public void reload(){
		this.reloadConfig();
		this.menu_book = this.get_menu();
	}
	
	private Menu get_menu(){
		
		Menu menu  = new Menu(this, getConfig().getString("inventoryname"), getConfig().getInt("inventoryrows"));
		
		if(getConfig() == null) return menu;
		
		ConfigurationSection booksconf = getConfig().getConfigurationSection("books");
		
		int i = 0;
		for(String key : booksconf.getKeys(false)){
			
			ConfigurationSection bookconf = booksconf.getConfigurationSection(key);
			
			ItemStack item = menu.createItem(Material.BOOK, bookconf.getInt("amount"), this.colorizeText(bookconf.getString("name")), this.colorizeText(bookconf.getString("lore")),(short) 0);
			
			menu.setItem(i,item);
			
			if(bookconf.getString("executeCommand") !="" && bookconf.getString("executeCommand") !=null){
				
				final String command = bookconf.getString("executeCommand") ;
				menu.setAction(i, new Menu.ItemAction(){

					@Override
					public void run(Player player, Inventory inv, ItemStack item, int slot, InventoryAction action) {
						ejecutarComando(player,command);
					}

				});
				
			}

			
			i++;
			
		}
		

		return menu;
		
	}
	
	private void ejecutarComando(Player player, String command) {
		//Parse player
		command = command.replace("&p", player.getName());
		//Execute command
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);		
	}
	
	public static String colorizeText(String string) {
	    string = string.replaceAll("&0", ChatColor.BLACK+"");
	    string = string.replaceAll("&1", ChatColor.DARK_BLUE+"");
	    string = string.replaceAll("&2", ChatColor.DARK_GREEN+"");
	    string = string.replaceAll("&3", ChatColor.DARK_AQUA+"");
	    string = string.replaceAll("&4", ChatColor.DARK_RED+"");
	    string = string.replaceAll("&5", ChatColor.DARK_PURPLE+"");
	    string = string.replaceAll("&6", ChatColor.GOLD+"");
	    string = string.replaceAll("&7", ChatColor.GRAY+"");
	    string = string.replaceAll("&8", ChatColor.DARK_GRAY+"");
	    string = string.replaceAll("&9", ChatColor.BLUE+"");
	    string = string.replaceAll("&a", ChatColor.GREEN+"");
	    string = string.replaceAll("&b", ChatColor.AQUA+"");
	    string = string.replaceAll("&c", ChatColor.RED+"");
	    string = string.replaceAll("&d", ChatColor.LIGHT_PURPLE+"");
	    string = string.replaceAll("&e", ChatColor.YELLOW+"");
	    string = string.replaceAll("&f", ChatColor.WHITE+"");
	    string = string.replaceAll("&l", ChatColor.BOLD+"");
	    return string;
	}
	

}
