package ar.com.rockcodes.rockbookshelf.listener;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import ar.com.rockcodes.rockbookshelf.RockBookShelf;
import ar.com.rockcodes.rockbookshelf.util.VaultHelper;


public class BookShelfListener implements Listener {
	
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
	  Player player = e.getPlayer();
	  Block block = e.getClickedBlock();
  		if(block !=null && e.getAction()!=null){
			  if(block.getType().equals(Material.BOOKSHELF) && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
				  
				  if(VaultHelper.permission.has(player, "rockbookshelf.click")){
					  RockBookShelf.plugin.menu_book.showMenu(player);  
				  }
				  
				  
			  }  
		  }
	}
	
	
}

